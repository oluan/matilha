# Tasks Manager #
## Gerenciamento de tarefas ##
## Desenvolvido por Luan Andrade (2016) para o desafio do Estúdio Matilha ##

### Features ###

* Aplicação em Ionic (ionicframework.com)
* REST API com Ruby on Rails (http://rubyonrails.org/)
* Documentação com swagger-docs (https://github.com/richhollis/swagger-docs)
* Layout com ionic-material (ionicmaterial.com)

### Diretórios ###

* /db - banco de dados para importação
* /screens - screens do app e da documentação
* /taskmanager - rest api ror
* /tasksApp - aplicativo ionic

### Como rodar ###

* Importar/migrar o banco de dados
* iniciar a api (rails server no diretório taskmanager)
* api roda em 127.0.0.1:3000/api/v1. Exemplo de requisição: 127.0.0.1:3000/api/v1/tasks - retorna todas as tasks
* Iniciar o aplicativo (ionic serve no diretório tasksApp)
* Aplicativo roda em http://127.0.0.1:8100/#/app/activity
* Documentação swagger da API roda em : http://127.0.0.1:3000/api/v1/