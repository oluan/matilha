require 'spec_helper'
require 'rails_helper'
require 'support/api/helper'

describe '/api/v1/tasks', :type => :api do

	let(:url) { "/api/v1/tasks" }
  	
	it "create" do
		# método HTTP sobre a URL especificada
		post "#{url}.json", :task => {
			:title => "Criar API Rails",
			:status => 1,
			:date => '2016-05-26 16:00:01' 
		}
		
		task_json = JSON.parse last_response.body 
		#print "\n***"+url.to_s+"***\n"
		expect(201).to eq(last_response.status)

	end
	
	it "update" do
		task_criada = Task.create(:title => "Criar API Rails", :status => 1, :date =>"2016-05-26 16:00:01")
		
		# método HTTP sobre a URL especificada
		put "#{url}/#{task_criada.id}.json", :task => {
			:title => "Task editada",
			:status => 0,
			:date => '2016-05-26 17:00:01' 
		}
		
		expect(204).to eq(last_response.status)

		
	end
	
end