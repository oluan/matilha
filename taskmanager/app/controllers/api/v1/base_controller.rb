class Api::V1::BaseController < ActionController::Base
	respond_to :json
	before_filter :permitir_parametros

	class << self
		Swagger::Docs::Generator::set_real_methods

		def inherited(subclass)
	      super
	      subclass.class_eval do
	        setup_basic_api_documentation
	      end
	    end
	    private
	    def setup_basic_api_documentation
	      [:index, :show, :create, :update, :delete].each do |api_action|
	        swagger_api api_action do
	          param :header, 'Authentication-Token', :string, :required, 'Authentication token'
	        end
	      end
	    end
	end

  	# No Rails 4 existe um mecanismo que regula o envio de parametros
  	# Utilizamos esse filtro para permitir o envio de qualquer tamanho de parametro
  	def permitir_parametros
  		params.permit!
  	end

end