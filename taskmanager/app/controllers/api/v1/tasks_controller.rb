class Api::V1::TasksController < Api::V1::BaseController

	swagger_controller :tasks, "Tasks"

	before_filter :cors_preflight_check
	after_filter :cors_set_access_control_headers

	
	def cors_set_access_control_headers
    	headers['Access-Control-Allow-Origin'] = '*'
    	headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, OPTIONS'
    	headers['Access-Control-Max-Age'] = "1728000"
  	end
  	def cors_preflight_check
	    headers['Access-Control-Allow-Origin'] = '*'
	    headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, OPTIONS'
	    headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
	    headers['Access-Control-Max-Age'] = '1728000'
	    #render :text => '', :content_type => 'text/plain'
  	end

	def index
		tasks = Task.all
		render json: tasks
	end

	def create
		task = Task.create(params[:task])
		if task.valid?
			respond_with(task, :location => api_v1_task_path(task))

		else
			respond_with(task)
		end
	end
  
	def update
		task = Task.update(params[:id],params[:task])

		if task.valid?
			respond_with(task, :location => api_v1_task_path(task))
		else
			respond_with(task)
		end
	end

	def show
		task = Task.find_by_id(params[:id])
		render json: task
	end

	# swagger-docs
	swagger_api :index do 
	    summary "Fetches all Tasks items"
	    notes "This lists all the tasks"
	    response :unauthorized
	    response :success
	end

  	swagger_api :show do 
	    summary "Fetches a single Task item"
	    param :path, :id, :integer, :optional, "Task Id"
	    response :ok, "Success", :User
	    response :unauthorized
	    response :not_acceptable
	    response :not_found
  	end

  	swagger_api :create do 
	    summary "Creates a new Task"
	    param :form, :title, :string, :required, "Title"
	    param :form, :date, :datetime, :required, "Date/Time"
	    param :form, :status, :integer, :required, "Status {0/1}"
	    param_list :form, :role, :string, :required, "Role", [ "admin", "superadmin", "user" ]
	    response :unauthorized
	    response :not_acceptable
  	end

  	swagger_api :update do
	    summary "Updates an existing Task"
	    param :path, :id, :integer, :required, "Task Id"
	    param :form, :title, :string, :optional, "Title"
	    param :form, :date, :datetime, :optional, "Date/Time"
	    param :form, :email, :integer, :optional, "Status{0/1}"
	    response :unauthorized
	    response :not_found
	    response :not_acceptable
  	end

end