angular.module('starter.services', [])

.factory('Task', function($resource) {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var tasks = [];

  return $resource('http://127.0.0.1:3000/api/v1/tasks/:id.json',{
    id: '@id'
  }, {
    'index': {
      method: 'GET',
      isArray: true
    },
    'show': {
      method: 'GET',
      isArray: false
    },
    'create': {
      method: 'POST',
      //transformRequest: nestData
    },
    'save': {
      method: 'PUT',
      //transformRequest: nestData
    },
    'destroy': {
      method: 'DELETE'
    }
  });
});
