/* global angular, document, window */
'use strict';

angular.module('starter.controllers', [])


.controller('ActivityCtrl', function($scope, $stateParams, $http, $timeout,$window, ionicMaterialMotion, ionicMaterialInk,Task) {

    //getting all tasks from api
    $http({
            method: 'GET',
            url: 'http://127.0.0.1:3000/api/v1/tasks',
            isArray:true
    })
    .then(function successCallback(response) {
        var tasks = response.data;
            
        for (var i=0;i<tasks.length; i++){
            if(tasks[i].status==1){
                tasks[i]['class']='balanced';
                tasks[i]['img']='done.png';
            } else {
                tasks[i]['class']='assertive';
                tasks[i]['img']='todo.png';
            }
        }

        $scope.tasks = tasks;

        }, function errorCallback(response) {
            console.log(response);
    });


    //adding data into api
    $scope.add = function (task) {
        var settings = {
              url: "http://127.0.0.1:3000/api/v1/tasks.json",
              method: "POST",
              headers: {
                "content-type": "application/json",
            },
            data: {"title": task.title, date:task.date,status:0}
        };

        console.log(task);
        $http(settings).then(function successCallback(response){
            alert("Task created");
            console.log(response);
            $window.location.href ="/"; 
        }, function errorCallback(response) {
            alert("Error: "+response.status);
            console.log("Erro: "+response.status);
        });

    };

$scope.update = function(task){
    if(task.status==1)
        var status = 0;
    else
        var status = 1;

    var settings = {
              url: "http://127.0.0.1:3000/api/v1/tasks/"+task.id+".json",
              method: "PUT",
              headers: {
                "Content-Type": "application/json;charset=UTF-8",
            },
            data: {status:status}
        };

        console.log(task);
        $http(settings).then(function successCallback(response){
            alert("Task changed");
            console.log(response);
            $window.location.href ="/"; 
        }, function errorCallback(response) {
            alert("Error: "+response.status);
            console.log("Erro: "+response.status);
        });
}

$timeout(function() {
    ionicMaterialMotion.fadeSlideIn({
        selector: '.animate-fade-slide-in .item'
    });
}, 200);

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
})

;
